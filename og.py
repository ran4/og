#!/usr/bin/env python3
import sys
usage_instructions = """Calculates ABV from OG and FG
Usage:
    
$ og 1.090 1.020
$ og 90 20

$ og
OG: 90
FG: 20
9.1875

python3
>>> import og
>>> print(og.alc_from_og_fg(1.090, 1.020))
"""

def alc_from_og_fg(og, fg):
    """Calculates expected alcohol content from original gravity og and
    final gravity fg.
    og and fg can be given in either order (biggest number becomes og),
    looking like either 1.090 or 90.
    """
    if og < fg: #Switch if we mess up the directions
        fg, og = og, fg
        
    if og > 1.6 or fg > 1.6: #give og/fg as 1.090 or just 90
        og = 1 + og/1000
        fg = 1 + fg/1000
        
    return (og - fg) * 131.25
    
def test_alc_from_og():
    og, fg = 1.090, 1.020
    assert abs(alc_from_og_fg(og, fg) - 9.1875 ) < 0.01
    assert abs(alc_from_og_fg(fg, og) - 9.1875 ) < 0.01  # different call order
    assert abs(alc_from_og_fg(90, 20) - 9.1875 ) < 0.01  # different unit
    
    
def _get_float(label):
    while True:
        input_value = input(label)
        
        try:
            float_value = float(input_value)
            return float_value
        except ValueError:
            continue
        
def _get_og_and_fg_from_stdin_or_argv():
    if len(sys.argv) >= 2:
        if sys.argv[1].lower() in ["-h", "--help"]:
            print(usage_instructions)
            exit()
        return map(float, sys.argv[1:3])
    else:
        return (_get_float("OG: "), _get_float("FG: " ))
    
if __name__ == "__main__":
    test_alc_from_og()
    og, fg = _get_og_and_fg_from_stdin_or_argv()
    print("{0:.4f}".format(alc_from_og_fg(og, fg)))
